package net.lugdunon.state.account.defaults.blacklist;

import java.util.Iterator;
import java.util.List;

import net.lugdunon.state.State;
import net.lugdunon.state.account.IAccountBlacklist;
import net.lugdunon.util.FileUtils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DefaultAccountBlacklist implements IAccountBlacklist
{
	private static final Logger LOGGER         =LoggerFactory.getLogger(DefaultAccountBlacklist.class);
	private static final String BLACK_LIST_FILE="blacklist.txt";
	private List<String>        blacklist;
	private long                lastModified;
	
	@Override
	public void initBlacklist()
    {
		blacklist   =FileUtils.getLTSeparatedValues(BLACK_LIST_FILE);
		lastModified=FileUtils.lastModified        (BLACK_LIST_FILE);
    }

	@Override
	public boolean usesExternalChangePolling()
	{
		return(true);
	}

	@Override
    public void checkForExternalChanges()
    {
	    long lmCheck=FileUtils.lastModified(BLACK_LIST_FILE);
	    
	    if(lastModified < lmCheck)
	    {
	    	LOGGER.info("Blacklist modified externally, updating.");
	    	
	    	lastModified=lmCheck;
			blacklist   =FileUtils.getLTSeparatedValues(BLACK_LIST_FILE);
	    }
    }

	@Override
	public int blackListCount()
	{
		return(blacklist.size());
	}

	@Override
	public Iterator<String> blackListIterator()
	{
		return(blacklist.iterator());
	}

	@Override
	public boolean isBlackList()
	{
		return(blacklist.size() > 0);
	}

	@Override
	public boolean isBlackListed(String account)
	{
		if(State.instance().isSuperUser(account))
		{
			return(false);
		}
		
		for(String blAcc:blacklist)
		{
			if(blAcc.equals(account))
			{
				return(true);
			}
		}
		
		return(false);
	}

	@Override
	public void unBlackList(String account)
    {
		if(hasExternalModifications())
		{
	    	LOGGER.info("Blacklist modified externally, updating.");
	    	
			blacklist=FileUtils.getLTSeparatedValues(BLACK_LIST_FILE);
		}
		
	    blacklist.remove(account);

	    FileUtils.writeLTSeparatedValues(BLACK_LIST_FILE,blacklist);
	    
		lastModified=FileUtils.lastModified(BLACK_LIST_FILE);
    }

	@Override
	public void blackList(String account)
    {
		if(hasExternalModifications())
		{
	    	LOGGER.info("Blacklist modified externally, updating.");
	    	
			blacklist=FileUtils.getLTSeparatedValues(BLACK_LIST_FILE);
		}
		
		blacklist.add(account);
		
		State.instance().kickAccount(
			account,
			(String) State.instance().getWorld().getWorldConfigProperty(
				"account.blacklist.denied.message"
			)
		);
		
	    FileUtils.writeLTSeparatedValues(BLACK_LIST_FILE,blacklist);
	    
		lastModified=FileUtils.lastModified(BLACK_LIST_FILE);
    }

    private boolean hasExternalModifications()
    {
	    return(lastModified < FileUtils.lastModified(BLACK_LIST_FILE));
    }
}
